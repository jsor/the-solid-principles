The SOLID Principles
==

https://jsor.gitlab.io/the-solid-principles

Keyboard Navigation
--

* <kbd>SPACE</kbd>: Go forward to the next slide step
* <kbd>BACKSPACE</kbd>: Go backward to the previous slide step
* <kbd>r</kbd>: Repeat the current slide step
* <kbd>←</kbd>: Go forward to the next slide
* <kbd>→︎</kbd>: Go backward to the previous slide
* <kbd>f</kbd>: Toggle fullscreen
* <kbd>n</kbd>: Toggle speaker notes
* <kbd>i</kbd>: Toggle slide image in speaker notes window
