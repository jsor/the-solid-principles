async function toggleFullscreen(element) {
    if (document.fullscreenElement === element) {
        return document.exitFullscreen();
    }

    if (document.fullscreenElement) {
        await document.exitFullscreen();
    }

    return element.requestFullscreen();
}

export default function fullscreenPlugin(praesi) {
    praesi.registerKeyboardShortcut(
        'f',
        async () => {
            return toggleFullscreen(praesi.root());
        },
        'Toggle fullscreen'
    );
}
