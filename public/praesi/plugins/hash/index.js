function setSlideHash(id, index) {
    const newHash = '#' + id;

    if (newHash === window.location.hash) {
        return;
    }

    const number = index + 1;

    window.history[!window.location.hash ? 'replaceState' : 'pushState'](
        {
            slide: number
        },
        `Slide ${number}`,
        newHash
    );
}

export default async function hashPlugin(praesi) {
    praesi.root().addEventListener('praesi:slide', e => {
        setSlideHash(e.target.id, praesi.activeSlideIndex());
    });

    function gotoSlideByHash() {
        if (!window.location.hash) {
            return;
        }

        praesi.gotoSlideById(window.location.hash.substr(1)).catch(error => {
            console.error(error);
        });
    }

    window.addEventListener('hashchange', gotoSlideByHash);

    gotoSlideByHash();
}
