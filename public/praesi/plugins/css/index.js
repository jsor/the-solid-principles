function ensurePreventAnimation(element) {
    element.dataset.praesiOrigAnimationDuration = element.style.animationDuration;
    element.dataset.praesiOrigAnimationDelay = element.style.animationDelay;

    element.style.animationDuration = '0s';
    element.style.animationDelay = '0s';
}

function revertPreventAnimation(element) {
    if ('praesiOrigAnimationDuration' in element.dataset) {
        element.style.animationDuration = element.dataset.praesiOrigAnimationDuration;
        delete element.dataset.praesiOrigAnimationDuration;
    }

    if ('praesiOrigAnimationDelay' in element.dataset) {
        element.style.animationDelay = element.dataset.praesiOrigAnimationDelay;
        delete element.dataset.praesiOrigAnimationDelay;
    }

}

function resetNoSpace(element) {
    if (!element.dataset.praesiHadNoSpace) {
        return;
    }

    element.classList.add('praesi-no-space');
    delete element.dataset.praesiHadNoSpace;
}

function classNames(className) {
    return className
        .split(' ')
        .filter(s => s.trim() !== '');
}

const animateInRegex = /praesi-animate-.+-in/;
const animateOutRegex = /praesi-animate-.+-out/;

function addClass(element, className, {preventAnimation = false} = {}) {
    if (
        element.classList.contains('praesi-no-space') &&
        animateInRegex.test(className)
    ) {
        element.dataset.praesiHadNoSpace = '1';
        element.classList.remove('praesi-no-space');
    }

    if (
        !preventAnimation &&
        animateOutRegex.test(className)
    ) {
        element.addEventListener('animationend', function ae() {
            resetNoSpace(element);
            element.removeEventListener('animationend', ae);
        });
    }

    revertPreventAnimation(element);

    if (preventAnimation) {
        ensurePreventAnimation(element);
        resetNoSpace(element);
    }

    element.classList.add(...classNames(className));
}

function removeClass(element, className, {preventAnimation = false} = {}) {
    revertPreventAnimation(element);

    if (preventAnimation) {
        ensurePreventAnimation(element);
        resetNoSpace(element);
    }

    element.classList.remove(...classNames(className));
}

function styleNames(style) {
    return style
        .split(';')
        .map(s => s.split(':')[0])
        .filter(s => s.trim() !== '');
}

function addStyle(element, style) {
    element.style.cssText += style;
}

function removeStyle(element, style) {
    styleNames(style).forEach(name => {
        element.style.removeProperty(name);
    });
}

export default function cssPlugin(praesi) {
    praesi.registerStepAction(
        'add-class',
        addClass,
        removeClass
    );

    praesi.registerStepAction(
        'remove-class',
        removeClass,
        addClass
    );

    praesi.registerStepAction(
        'swap-class',
        (el, className, options = {}) => {
            const [add, remove] = className.split(':');

            removeClass(el, remove, options);
            addClass(el, add, options);
        },
        (el, className, options = {}) => {
            const [add, remove] = className.split(':');

            removeClass(el, add, options);
            addClass(el, remove, options);
        }
    );
    praesi.registerStepAction(
        'add-style',
        addStyle,
        removeStyle
    );

    praesi.registerStepAction(
        'remove-style',
        removeStyle,
        addStyle
    );
}
