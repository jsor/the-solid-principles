export default async function notesPlugin(praesi) {
    const localStorageKeyDetect = `praesi-notes-are-you-there`;
    const localStorageKeyReload = `praesi-notes-reload-me`;
    const localStorageKeySlideImage = `praesi-notes-show-slide-image`;

    let notesWindow;
    let isOpening = false;
    let showSlideImage = localStorage.getItem(localStorageKeySlideImage) !== 'no';

    async function detectNotesWindow() {
        return new Promise(resolve => {
            function listener({key, newValue}) {
                if (key === localStorageKeyDetect && newValue === 'yes') {
                    resolve(true);
                }
            }

            window.addEventListener('storage', listener);
            localStorage.setItem(localStorageKeyDetect, 'ask');

            setTimeout(() => {
                resolve(false);
                localStorage.setItem(localStorageKeyDetect, 'no');
                window.removeEventListener('storage', listener);
            }, 200);
        });
    }

    function openNotes() {
        isOpening = true;

        notesWindow = window.open(
            '',
            `praesi-notes`,
            'menubar=no,toolbar=no,location=no,status=no'
        );

        notesWindow.document.open();

        const {cssUrls = []} = praesi.option('notes');

        notesWindow.document.write(`
            <title>Notes: ${window.document.title}</title>
            ${cssUrls.map(url => {
                return `<link rel="stylesheet" href=${url}>`;
            }).join('')}
            <script>
            window.addEventListener('storage', ({key, newValue}) => {
                if (key === ${JSON.stringify(localStorageKeyDetect)} && newValue === 'ask') {
                  window.localStorage.setItem(key, 'yes');
                }
            });
            window.localStorage.setItem(${JSON.stringify(localStorageKeyReload)}, Math.random() + '');
            </script>
        `);

        notesWindow.document.close();

        notesWindow.addEventListener('keydown', event => {
            praesi.handleKeyEvent(event);
        });

        notesWindow.addEventListener('load', () => {
            isOpening = false;
        });

        notesWindow.addEventListener('beforeunload', () => {
            notesWindow = null;
        });

        setSlideNotes();
        setStepNotes();
        setStepImage();
    }

    function toggleNotes() {
        if (notesWindow) {
            notesWindow.close();
            notesWindow = null;
            return;
        }

        openNotes();
    }

    function setSlideNotes() {
        if (!notesWindow) {
            return;
        }

        const numSteps = praesi.activeSlideSteps();
        const stepHtml = numSteps ? ` (<span class="praesi-notes-step">0</span> / ${numSteps})` : '';
        const slideImageClass = showSlideImage ? ` praesi-notes-has-slide-image` : '';

        let html = `<div class="praesi-notes${slideImageClass}"><div class="praesi-notes-text"><h1>Slide ${praesi.activeSlideIndex() + 1}${stepHtml}</h1>`;

        const notes = praesi.activeSlideElement().querySelector('.praesi-notes');

        if (notes) {
            html += notes.innerHTML;
        }

        html += '</div>';

        if (showSlideImage) {
            html += '<div class="praesi-notes-slide-image"><img src="" alt=""></div></div>';
        }

        html += '</div>';

        notesWindow.document.body.innerHTML = html;
    }

    function setStepNotes() {
        if (!notesWindow) {
            return;
        }

        const stepElement = notesWindow.document.querySelector('.praesi-notes-step');

        if (stepElement) {
            stepElement.innerHTML = praesi.activeStepIndex();
        }
    }

    function setStepImage() {
        if (!notesWindow || !showSlideImage) {
            return;
        }

        if (!domtoimage) {
            return;
        }

        setTimeout(() => {
            domtoimage.toSvg(praesi.activeSlideElement(), {width: 960, height: 540})
                .then(function (dataUrl) {
                    if (!showSlideImage) {
                        return;
                    }

                    const imageElement = notesWindow.document.querySelector('.praesi-notes-slide-image img');

                    if (imageElement) {
                        imageElement.src = dataUrl;
                    }
                })
                .catch(function (error) {
                    console.error('Rendering step image failed!', error);
                });
        }, 0);
    }

    praesi.root().addEventListener(
        'praesi:slide',
        setSlideNotes
    );

    praesi.root().addEventListener(
        'praesi:step',
        setStepNotes
    );
    praesi.root().addEventListener(
        'praesi:step',
        setStepImage
    );

    praesi.root().addEventListener(
        'praesi:step',
        () => {
            praesi.root().addEventListener(
                'animationend',
                function ae() {
                    setStepImage();
                    praesi.root().removeEventListener('animationend', ae);
                }
            );
        }
    );

    praesi.root().addEventListener(
        'animationend',
        setStepImage
    );

    praesi.registerKeyboardShortcut(
        'n',
        toggleNotes,
        'Toggle speaker notes'
    );

    praesi.registerKeyboardShortcut(
        'i',
        () => {
            showSlideImage = !showSlideImage;
            localStorage.setItem(localStorageKeySlideImage, showSlideImage ? 'yes' : 'no');
            setSlideNotes();
            setStepNotes();
            setStepImage();
        },
        'Toggle slide image in speaker notes window'
    );

    window.addEventListener('storage', ({key}) => {
        if (!isOpening && key === localStorageKeyReload) {
            openNotes();
        }
    });

    if (await detectNotesWindow()) {
        openNotes();
    }
}
