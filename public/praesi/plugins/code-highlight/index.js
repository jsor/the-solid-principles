function highlight(element, line, {preventAnimation = false} = {}) {
    const code = element.querySelector('code');
    let bar = element.querySelector('.praesi-code-highlight-bar');

    if (!bar) {
        bar = document.createElement('span');
        bar.classList.add('praesi-code-highlight-bar');

        element.prepend(bar);
    }

    let y = 0;

    bar.style.opacity = '0';
    bar.style.top = '0';

    if (0 !== Number(line)) {
        const elPaddingTop = parseInt(getComputedStyle(element).paddingTop, 10);
        const codeLineHeight = parseInt(getComputedStyle(code).lineHeight, 10);
        const codePaddingTop = parseInt(getComputedStyle(code).paddingTop, 10);

        const topLimit = 3 * codeLineHeight;
        let top = codePaddingTop + elPaddingTop;
        let factor = 1;

        while (true) {
            if (top + codeLineHeight > topLimit) {
                break;
            }

            top += codeLineHeight;
            factor++;
        }

        y = ((Number(line) - factor) * -codeLineHeight);

        bar.style.opacity = '1';
        bar.style.height = codeLineHeight + 'px';
        bar.style.top = top + 'px';
    }

    if (!preventAnimation) {
        code.style.transitionProperty = 'transform';
        code.style.transitionTimingFunction = 'ease-in-out';
        code.style.transitionDuration = '.3s';
        code.offsetLeft; // style recalc

        code.addEventListener('transitionend', function te() {
            code.style.transitionProperty = '';
            code.style.transitionTimingFunction = '';
            code.style.transitionDuration = '';

            code.removeEventListener('transitionend', te);
        });
    }

    code.style.transform = 'translateY(' + y + 'px)';
}

export default function codeHighlightPlugin(praesi) {
    praesi.registerStepAction(
        'code-highlight',
        (element, line, options) => {
            if (!element.praesiLineStack) {
                element.praesiLineStack = [];
            }

            element.praesiLineStack.unshift(line);

            highlight(element, line, options);
        },
        (element, line, options) => {
            if (!element.praesiLineStack) {
                element.praesiLineStack = [];
            }

            element.praesiLineStack.shift();

            const previousLine = element.praesiLineStack.length > 0 ? element.praesiLineStack[0] : 0;

            highlight(element, previousLine, options);
        },
    )
}
