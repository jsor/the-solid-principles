const plugins = [];

praesi.plugin = plugin => {
    plugins.push(plugin);
};

async function dispatch(element, type, eventInit = {}) {
    return new Promise(resolve => {
        element.addEventListener(type, function check() {
            resolve();
            element.removeEventListener(type, check);
        });

        element.dispatchEvent(new CustomEvent(type, eventInit));
    });
}

const stepAttributeRegex = /^data-step-(\d+)(-(.+))?$/;

function extractSlideSteps(slide) {
    return Array.from(slide.querySelectorAll('.praesi-step')).map(el => {
        return Array.from(el.attributes).reduce((steps, attr) => {
            let matches = attr.name.match(stepAttributeRegex);

            if (matches && matches[1]) {
                steps.push({
                    num: Number(matches[1]),
                    action: matches[3],
                    arg: attr.value,
                    el: el
                });
            }

            return steps;
        }, []).sort((a, b) => {
            return b.num - a.num;
        });
    }, 0);
}

function maxSlideSteps(slide) {
    return extractSlideSteps(slide).reduce((curr, steps) => {
        return Math.max(curr, steps.reduce((curr, step) => {
            return Math.max(curr, step.num);
        }, curr));
    }, 0);
}

const rafIds = [];

function frame(callback) {
    rafIds.push(
        requestAnimationFrame(callback)
    );
}

function resetFrames() {
    while (rafIds.length > 0) {
        cancelAnimationFrame(rafIds.pop());
    }
}

export default async function praesi(options = {}) {
    let instance;
    let activeSlideIndex = -1;
    let activeStepIndex = -1;

    const stepActions = {};

    const keyboardShortcuts = [];

    const root = document.querySelector('.praesi');
    const stage = root.querySelector('.praesi-stage');
    const slides = Array.from(root.querySelectorAll('.praesi-slide'));

    slides.forEach((slide, i) => {
        if (!slide.id) {
            slide.id = 'slide' + (i + 1);
        }
    });

    root.classList.add('praesi-ready');
    root.classList.remove('no-praesi');

    const rootRect = stage.getBoundingClientRect();

    function resize() {
        const viewportWidth = Math.max(
            document.documentElement.clientWidth,
            window.innerWidth
        );
        const viewportHeight = Math.max(
            document.documentElement.clientHeight,
            window.innerHeight
        );

        const stageScale = Math.min(
            viewportWidth / rootRect.width,
            viewportHeight / rootRect.height
        );

        const stageLeft = (viewportWidth - (rootRect.width * stageScale)) / 2;
        const stageTop = (viewportHeight - (rootRect.height * stageScale)) / 2;

        stage.style.transform = `translate(${stageLeft}px, ${stageTop}px) scale(${stageScale})`;
    }

    window.addEventListener('resize', resize);
    resize();

    function iterateSlideSteps(slide, callback, {reverseActions = false} = {}) {
        for (let actions of extractSlideSteps(slide)) {
            if (reverseActions) {
                actions = actions.reverse();
            }

            for (const step of actions) {
                const {num, action, arg, el} = step;

                if (!stepActions[action]) {
                    continue;
                }

                callback(num, stepActions[action], arg, el);
            }
        }
    }

    function hasSlide(index) {
        if (0 === slides.length) {
            return false;
        }

        if (index < 0) {
            index = slides.length + index;
        }

        return index >= 0 && index < slides.length;
    }

    async function slide(index) {
        if (index === activeSlideIndex) {
            return;
        }

        if (-1 !== activeSlideIndex) {
            const currentSlide = slides[activeSlideIndex];

            const resetSlideSteps = () => {
                iterateSlideSteps(currentSlide, (num, action, arg, el) => {
                    action['reset'](el, arg, {preventAnimation: true});
                });
            };

            const style = getComputedStyle(currentSlide);

            if (
                style.transitionDuration === '0s' &&
                style.transitionDelay === '0s'
            ) {
                resetSlideSteps();
            } else {
                currentSlide.addEventListener('transitionend', function te() {
                    resetSlideSteps();
                    currentSlide.removeEventListener('transitionend', te);
                });
            }
        }

        activeSlideIndex = index;

        for (const slide of slides) {
            slide.classList.remove('praesi-slide-active');
        }

        slides[index].classList.add('praesi-slide-active');

        await dispatch(slides[index], 'praesi:slide', {
            detail: {
                praesi: instance
            },
            bubbles: true
        });
    }

    function hasStep(index) {
        const maxStep = maxSlideSteps(slides[activeSlideIndex]);

        if (0 === maxStep) {
            return false;
        }

        if (index < 0) {
            index = maxStep + index + 1;
        }

        return index >= 0 && index <= maxStep;
    }

    async function step(index) {
        const slide = slides[activeSlideIndex];

        if (index < 0) {
            index = maxSlideSteps(slide) + index + 1;
        }

        if (-1 === activeStepIndex) {
            // Forward from other slide
            if (0 === index) {
                iterateSlideSteps(slide, (num, action, arg, el) => {
                    if (0 !== num) {
                        return;
                    }

                    action['execute'](el, arg);
                });
            }

            // Backward from other slide
            else {
                iterateSlideSteps(slide, (num, action, arg, el) => {
                    action['execute'](el, arg, {preventAnimation: true});
                }, {reverseActions: true});
            }
        }

        // Repeat current step
        else if (activeStepIndex === index) {
            iterateSlideSteps(slide, (num, action, arg, el) => {
                if (num !== index) {
                    return;
                }

                action['reset'](el, arg, {preventAnimation: true});

                frame(() => {
                    frame(() => {
                        action['execute'](el, arg);
                    })
                });
            });
        }

        // Forward from previous step
        else if (activeStepIndex === index - 1) {
            iterateSlideSteps(slide, (num, action, arg, el) => {
                if (num !== index) {
                    return;
                }

                action['execute'](el, arg);
            });
        }

        // Backward from previous step
        else if (activeStepIndex === index + 1) {
            iterateSlideSteps(slide, (num, action, arg, el) => {
                if (num !== activeStepIndex) {
                    return;
                }

                action['reset'](el, arg, {preventAnimation: true});
            });
        }

        activeStepIndex = index;

        await dispatch(slide, 'praesi:step', {
            detail: {
                praesi: instance
            },
            bubbles: true
        });
    }

    class Praesi {
        options() {
            return Object.assign({}, options);
        }

        option(key) {
            const {[key]: value} = this.options();
            return value;
        }

        root() {
            return root;
        }

        registerStepAction(action, execute, reset) {
            stepActions[action] = {execute, reset};
        }

        registerKeyboardShortcut(key, handler, help) {
            keyboardShortcuts.push({key, handler, help});
        }

        activeSlideElement() {
            return slides[activeSlideIndex];
        }

        activeSlideIndex() {
            return activeSlideIndex;
        }

        activeStepIndex() {
            return activeStepIndex;
        }

        activeSlideSteps() {
            return maxSlideSteps(slides[activeSlideIndex]);
        }

        async repeat() {
            if (hasStep(activeStepIndex)) {
                resetFrames();

                return step(activeStepIndex);
            }

            if (!hasSlide(activeSlideIndex)) {
                return;
            }

            resetFrames();

            return slide(activeSlideIndex);
        }

        async next() {
            if (hasStep(activeStepIndex + 1)) {
                resetFrames();

                return step(activeStepIndex + 1);
            }

            if (!hasSlide(activeSlideIndex + 1)) {
                return;
            }

            resetFrames();

            activeStepIndex = -1;

            return Promise.all([
                slide(activeSlideIndex + 1),
                step(0)
            ]);
        }

        async prev() {
            if (activeStepIndex > 0 && hasStep(activeStepIndex - 1)) {
                resetFrames();

                return step(activeStepIndex - 1);
            }

            if (activeSlideIndex <= 0 || !hasSlide(activeSlideIndex - 1)) {
                return;
            }

            resetFrames();

            activeStepIndex = -1;

            return Promise.all([
                slide(activeSlideIndex - 1),
                step(-1)
            ]);
        }

        async nextSlide() {
            if (!hasSlide(activeSlideIndex + 1)) {
                return;
            }

            resetFrames();

            activeStepIndex = -1;

            return Promise.all([
                slide(activeSlideIndex + 1),
                step(0)
            ]);
        }

        async prevSlide() {
            if (activeSlideIndex <= 0 || !hasSlide(activeSlideIndex - 1)) {
                return;
            }

            resetFrames();

            activeStepIndex = -1;

            return Promise.all([
                slide(activeSlideIndex - 1),
                step(0)
            ]);
        }

        async gotoSlide(index) {
            if (!hasSlide(index)) {
                return;
            }

            resetFrames();

            activeStepIndex = -1;

            return Promise.all([
                slide(index),
                step(0)
            ]);
        }

        async gotoSlideById(id) {
            const index = slides.reduce((curr, slide, i) => {
                return id === slide.id ? i : curr;
            }, null);

            return this.gotoSlide(index);
        }

        handleKeyEvent(event) {
            // Only single keys supported for now
            if (event.altKey || event.ctrlKey || event.metaKey) {
                return;
            }

            for (const shortcut of keyboardShortcuts) {
                const {key, handler} = shortcut;

                if (key !== event.key) {
                    continue;
                }

                event.preventDefault();

                new Promise(resolve => {
                    resolve(handler(event));
                }).catch(error => {
                    console.error(error);
                });
            }
        }
    }

    instance = new Praesi();

    instance.registerKeyboardShortcut(
        '?',
        () => {
            alert('TBD');
        },
        'Open the help'
    );

    instance.registerKeyboardShortcut(
        ' ',
        instance.next.bind(instance),
        'Go forward to the next slide step'
    );
    instance.registerKeyboardShortcut(
        'Backspace',
        instance.prev.bind(instance),
        'Go backward to the previous slide step'
    );
    instance.registerKeyboardShortcut(
        'r',
        instance.repeat.bind(instance),
        'Repeat the current slide step'
    );
    instance.registerKeyboardShortcut(
        'ArrowRight',
        instance.nextSlide.bind(instance),
        'Go forward to the next slide'
    );
    instance.registerKeyboardShortcut(
        'ArrowLeft',
        instance.prevSlide.bind(instance),
        'Go backward to the previous slide'
    );

    await Promise.all(plugins.map(init => init(instance)));

    window.addEventListener('keydown', instance.handleKeyEvent.bind(instance));

    if (activeSlideIndex === -1) {
        const goto = slides.reduce((curr, slide, i) => {
            if (!slide.classList.contains('praesi-slide-active')) {
                return curr;
            }

            return i;
        }, 0);

        await instance.gotoSlide(goto);
    }

    return instance;
}
